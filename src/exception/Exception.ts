/**
 * @author Dmitro Zataidukh
 * @email zidadindimon@gmail.com
 * @createdAt 4/25/20
 */

/* tslint:disable:max-classes-per-file */

export class Exception  extends Error {
  readonly status: number;
}

export class NotFoundException extends Exception {
  readonly status = 404
}
