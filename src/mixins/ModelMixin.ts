/**
 * @author Dmytro Zataidukh
 * @email dz@letyshops.com
 * @created_at 28.02.20
 */
import { Component, Prop, Vue, Watch } from 'vue-property-decorator';
import { Model, TObject } from '@zidadindimon/vue-mc/src';
import { Exception, NotFoundException } from '@/exception/Exception';

export const ModelMixin = <T extends Model, F = TObject>(model: { new(): T }) => {

  @Component({})
  class Form extends Vue {
    @Prop() id: number;
    notFound: boolean = false;
    model: T = new (model as { new(): T })();
    editing: boolean = false;

    @Watch('id', { immediate: true })
    async fetch() {
      this.notFound = false;
      if (!this.id) {
        return;
      }

      try {
        await this.model.fetch({ id: this.id }, false);
      } catch (error) {
        this.notFound = (error instanceof NotFoundException);
        this.onError(error, 'fetch');
      }
    }

    get attrsError() {
      return this.model.errors.attrs || {};
    }

    get rules() {
      return this.model.rules();
    }

    async save() {
      try {
        await this.model.save();
        this.editing = false;
        this.onSave();
      } catch (error) {
        this.onError(error, 'save');
      }
    }

    onSave() {
    }

    async onError(error: Exception, type?: 'save' | 'fetch') {
    }
  }

  return Form;
};
