import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '../views/Home.vue';
import UserView from '@/views/UserView.vue';
import { NRoute } from '@/router/NRoute';
import UserIndexView from '@/views/UserIndexView.vue';
import UserListView from '@/views/UserListView.vue';
import TaskIndexView from '@/views/TaskIndexView.vue';
import TaskView from '@/views/TaskView.vue';
import TaskListView from '@/views/TaskListView.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/task',
    component: TaskIndexView,
    children: [
      {
        path: '',
        name: NRoute.TASK_LIST,
        component: TaskListView,
      },
      {
        path: 'view/:id',
        name: NRoute.TASK,
        component: TaskView,
        props: route => ({ id: Number(route.params.id) }),
      },
      {
        path: 'create',
        name: NRoute.TASK_CREATE,
        component: TaskView,
      },
    ],
  },
  {
    path: '/user',
    component: UserIndexView,
    children: [
      {
        path: '',
        name: NRoute.USER_LIST,
        component: UserListView,
      },
      {
        path: 'view/:id',
        name: NRoute.USER,
        component: UserView,
        props: route => ({ id: Number(route.params.id) }),
      },
      {
        path: 'create',
        name: NRoute.USER_CREATE,
        component: UserView,
      },
    ],
  },
] as RouteConfig[];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;
