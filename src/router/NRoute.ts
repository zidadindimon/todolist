/**
 * @author Dmitro Zataidukh
 * @email zidadindimon@gmail.com
 * @createdAt 4/25/20
 */

export enum NRoute {
  USER = 'user',
  USER_CREATE = 'userCreate',
  USER_LIST = 'userList',
  TASK = 'task',
  TASK_CREATE = 'taskCreate',
  TASK_LIST = 'taskList',
}
