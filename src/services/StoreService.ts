/**
 * @author Dmitro Zataidukh
 * @email zidadindimon@gmail.com
 * @createdAt 4/25/20
 */
import { TModelData } from '@/types/IGeneral';
import { NotFoundException } from '@/exception/Exception';

export class StoreService {
  private static getData<T>(key: string): T {
    const data = localStorage.getItem(key);

    return data ? JSON.parse(data) : null;
  }

  private static setData<T>(key: string, data: T): void {
    return localStorage.setItem(key, JSON.stringify(data));
  }

  private static findModelInArr(list: TModelData[], { id }: TModelData) {
    const idx = list.findIndex(item => item.id === id);
    if (idx === -1) {
      throw new NotFoundException('Content not found');
    }
    return idx;
  }

  static getList<T extends TModelData>(key: string): T[] {
    return this.getData<T[]>(key) || [] as T[];
  }

  static setToList<T extends TModelData>(key: string, data: T): T {
    const list = this.getList<T>(key);

    if (!data.id) {
      data.id = list.length + 1;
      list.push(data);
    } else {
      const idx = this.findModelInArr(list, data);
      list[idx] = data;
    }
    this.setData(key, list);
    return data;
  }

  static getFromList<T extends TModelData>(key: string, id: number): T {
    const list = this.getList<T>(key);
    return list[this.findModelInArr(list, { id })];
  }

  static removeFromList(key: string, id: number): void {
    const list = this.getList<any>(key);
    list.splice(this.findModelInArr(list, { id }), 1);
  }


}
