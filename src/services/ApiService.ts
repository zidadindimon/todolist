/**
 * @author Dmitro Zataidukh
 * @email zidadindimon@gmail.com
 * @createdAt 4/24/20
 */
import { TUser } from '@/mc/model/User';
import { StoreService } from '@/services/StoreService';
import { TModelData } from '@/types/IGeneral';
import { delay } from '@/helper/until';

export class ApiService {
  static async userCreate(user: TUser) {
    await delay(Math.floor(Math.random() * 1500) + 500);
    return StoreService.setToList('user', user);
  }

  static async userUpdate(user: TUser) {
    await delay(Math.floor(Math.random() * 1500) + 500);
    return StoreService.setToList('user', user);
  }

  static async userFetch(data: TModelData) {
    return StoreService.getFromList<TUser>('user', data.id);
  }

  static async userDelete(data: TModelData) {
    await delay(Math.floor(Math.random() * 1500) + 500);
    return StoreService.removeFromList('user', data.id);
  }
}

