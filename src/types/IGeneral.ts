/**
 * @author Dmitro Zataidukh
 * @email zidadindimon@gmail.com
 * @createdAt 4/25/20
 */
import { TObject } from '@zidadindimon/vue-mc';

export type TModelData = TObject & {
  id?: number;
}
