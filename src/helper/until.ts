/**
 * @author Dmitro Zataidukh
 * @email zidadindimon@gmail.com
 * @createdAt 4/25/20
 */

export const delay = (delay: number) => new Promise((resolve => setTimeout(resolve, delay)))
