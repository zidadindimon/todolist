/**
 * @author Dmitro Zataidukh
 * @email zidadindimon@gmail.com
 * @createdAt 4/24/20
 */

import { Model } from '@zidadindimon/vue-mc/src';
import { IModelApiProvider, TMutations, TRules } from '@zidadindimon/vue-mc';
import { ApiService } from '@/services/ApiService';
import { TModelData } from '@/types/IGeneral';
import { Exception, NotFoundException } from '@/exception/Exception';

export type TUser = TModelData & {
  username: string;
  email: string;
  avatar: string;
};

export class User extends Model<TUser, TUser, TModelData, TModelData> implements TUser{
  readonly id: number = null;
  username: string = 'anonymous';
  email: string = 'anonymous@mail.com';
  avatar: string = require('@/assets/anonymous.png');

  protected api(): IModelApiProvider<TUser, TUser, TModelData, TModelData> {
    return {
      save: ApiService.userCreate,
      update: ApiService.userUpdate,
      fetch: ApiService.userFetch,
      delete: ApiService.userDelete
    };
  }

  protected mutateBeforeSave(): TMutations<TUser> {
    return {
      id: this.id,
      username: this.username,
      email: this.email,
      avatar: this.avatar
    };
  }

  protected onError(exception: Exception) {
    super.onError(exception);
  }

  protected onSave(data: TUser): void {
    this.init(data, false)
  }

  rules(): TRules<User> {
    return {
      username: [
        v => !!v || 'Field Cannot be empty'
      ],
      email: [
        v => !!v || 'Field Cannot be empty',
        v => /^[A-z\d_\.]{2,}@[A-z\d_]{2,}\.[A-z\d]{2,}$/.test(v) || 'Email address not valid'
      ]
    };
  }

}
