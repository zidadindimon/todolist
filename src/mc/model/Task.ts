/**
 * @author Dmitro Zataidukh
 * @email zidadindimon@gmail.com
 * @createdAt 4/24/20
 */
import { Model } from '@zidadindimon/vue-mc/src';

export type TTask = {
  id: number;
  title: string;
  description: string;
  createdAt: string;
  dueDate: string;
  authorId: number;
  executorId: number;
}


export class Task extends Model {
  id: number;

}
