/**
 * @author Dmitro Zataidukh
 * @email zidadindimon@gmail.com
 * @createdAt 4/25/20
 */
import { Collection, ICollectionApiProvider, TFetchResp } from '@zidadindimon/vue-mc';
import { TUser, User } from '@/mc/model/User';
import { StoreService } from '@/services/StoreService';
import { delay } from '@/helper/until';

export type TUserFilter = {
  search?: string;
  page: number;
  size?: number;
}

export class UserCollection extends Collection<User, TUser, TUserFilter> {

  protected model(): { new(): User } {
    return User;
  }

  protected defFilterOpt(): TUserFilter {
    return {
      page: 0,
      size: 10,
      search: ''
    };
  }

  protected api(): ICollectionApiProvider<TUser, TUserFilter> {
    return {
      async fetch({ page, size, search }: TUserFilter): Promise<TFetchResp<TUser>> {
        await delay(250)
        const content = (await StoreService.getList<TUser>('user'))
          .filter(item => JSON.stringify(item).match(search || ''));
        const begin = size * page;
        return {
          content: content.slice(begin, begin + size),
          pages: Math.ceil(content.length / size),
          total: content.length,
        };
      },
    };
  }

}
